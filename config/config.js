const assert= require('assert')
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session)


//Session
let store;
if(process.env.NODE_ENV === 'development'){
    store = new session.MemoryStore;
}else{
    store = new MongoDBStore({
        uri: process.env.MONGO_URI,
        collection:'sessions'
    });
    store.on('error',(error)=>{
        assert.ifError(error);
        assert.ok(false);
    })
}
module.exports={store};