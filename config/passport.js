const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');




passport.use('local-signin',new LocalStrategy({
    usernameField:'email',
    passwordField:'password',
    passReqToCallback:true
},
    async(req, email, password, done) => {
        try {
            console.log("localstrategy",email,password);
            const user = await User.findOne({ email })
            if (!user) return done(null, false,{message:'The email or password are incorrect'});
            if (!user.validPassword(password)) return done(null, false,{message:'The email or password are incorrect'});
            return done(null, user);
        } catch (err) {
            return done(err);
        }
        
    }
));


passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: `${process.env.HOST}/auth/google/callback`
  },
  function(accessToken, refreshToken, profile, cb) {
        User.findOneOrCreateByGoogle(profile, function (err, user) {
        return cb(err, user);
        });
  }
));


passport.use(new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_APP_SECRET
  }, function(accessToken, refreshToken, profile, done) {
      try{
          console.log(profile)
        User.findOneOrCreateByFacebook(profile, function (err, user) {
            if(err) console.log(`Error: ${err}`);
            return done(err, user);
        });
      }catch(err2){
          console.log(`Error2: ${err2}`);
          return done(err2,null);
      }
  }
));

passport.serializeUser(function(user,done){
    done(null, user.id)
});

passport.deserializeUser(async(id,done)=>{
    const user = await User.findById(id);
    done(null,user)
});

module.exports = passport;

