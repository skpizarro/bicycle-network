const express = require('express');
const router = express.Router();
const {bicycle_list,bicycle_create, bicycle_delete,bicycle_update} = require('../../controllers/api/bicycleControllerAPI');

router.get('/',bicycle_list);
router.post('/create',bicycle_create);
router.delete('/delete',bicycle_delete);
router.put('/update',bicycle_update);


module.exports = router;