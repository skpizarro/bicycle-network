const express = require('express');
const router = express.Router();
const {users_list,user_create,user_reservation} = require('../../controllers/api/userControllerAPI');

router.get('/',users_list);
router.post('/create',user_create);
router.post('/reserve',user_reservation);

module.exports = router;

