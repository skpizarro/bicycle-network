const express = require('express')
const router = express.Router();
const passport = require('../../config/passport')
const {authenticate, forgotPassword,authFacebookToken} = require('../../controllers/api/authControllerAPI');


router.post('/authenticate',authenticate);
router.post('/forgotPassword',forgotPassword);
router.post('/facebook_token',passport.authenticate('facebook-token'),authFacebookToken)

module.exports = router;