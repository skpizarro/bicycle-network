const express = require('express');
const router = express.Router();
const {confirmationGet} = require('../controllers/token');

router.get('/confirmation/:token',confirmationGet);

module.exports = router;