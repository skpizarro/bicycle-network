const express = require('express');
const router = express.Router();
const {login_get,login_post,logout,forgotPass_get,forgotPass_post,resetPassword_get,resetPassword_post} =require('../controllers/login');

router.get('/login',login_get);
router.post('/login',login_post);
router.get('/logout',logout);
router.get('/login/forgotPassword',forgotPass_get);
router.post('/login/forgotPassword',forgotPass_post);
router.get('/resetPassword/:token',resetPassword_get)
router.post('/resetPassword/:token',resetPassword_post)

module.exports = router;