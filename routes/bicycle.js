const express = require('express');
const router = express.Router();

const {bicycle_list,bicycle_create_get,bicycle_create_post,bicycle_delete,bicycle_update_get,bicycle_update_post} = require('../controllers/bicycle');

router.get('/',bicycle_list);
router.get('/create',bicycle_create_get);
router.post('/create',bicycle_create_post);
router.get('/:id/update',bicycle_update_get);
router.post('/:id/update',bicycle_update_post);
router.post('/:id/delete',bicycle_delete);

module.exports = router;