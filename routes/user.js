var express = require('express');
var router = express.Router();
const {user_list,user_create_get,user_create,user_update_get,user_update,user_delete} = require('../controllers/user');


router.get('/',user_list);
router.get('/create',user_create_get);
router.post('/create',user_create);
router.get('/:id/update',user_update_get);
router.post('/:id/update',user_update);
router.post('/:id/delete',user_delete);





module.exports = router;
