var mongoose = require('mongoose');
var Bicycle = require('../../models/bicycle');
const { db } = require('../../models/bicycle');
const { bicycle_update } = require('../../controllers/api/bicycleControllerAPI');


describe('Testing bicycle',()=>{
    beforeEach((done)=>{
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser:true,useUnifiedTopology:true});
        const db = mongoose.connection;
        db.on('error',console.error.bind(console, 'Connection error'));
        db.once('open',function(){
            console.log('We are connected to test database!');
            done();
        })
    })

    afterEach((done)=>{
        Bicycle.deleteMany({},(err,success)=>{
            if(err) console.log(err);
            db.close();
            done();
        })
        
    })


    describe('Bicycle.createInstance',()=>{
        it('create a bicycle instance',(done)=>{
            var bicy = Bicycle.createInstance(1,'Black','Mountain',[6.765439, -75.888765]);

            expect(bicy.code).toBe(1);
            expect(bicy.color).toBe('Black');
            expect(bicy.model).toBe('Mountain');
            expect(bicy.location[0]).toEqual(6.765439);
            expect(bicy.location[1]).toEqual(-75.888765);
            done();
        })
    })

    describe('Bicycle.allBicycle',()=>{
        it('starts empty',(done)=>{
            Bicycle.allBicycle(function(err,bicys){
                expect(bicys.length).toBe(0)
                done();
            });
            
        })
    })


    describe('Bicycle.add',()=>{
        it('add a Bicy',(done)=>{
            Bicycle.allBicycle(function(err,bicys){
                expect(bicys.length).toBe(0)
                const bicy = Bicycle.createInstance(5,'Black','Mountain',[6.765439, -75.888765]);
                Bicycle.add(bicy,(err,newBicy)=>{
                    if(err) console.log(err);
                    Bicycle.allBicycle((err,bicys)=>{
                        expect(bicys.length).toBe(1);
                        expect(bicys[0].code).toEqual(bicy.code);
                        done();
                    })
                })
            
            });

            
        })
    })

    describe('Bicycle.findByCode',()=>{
        it('must return a bicycle with code 1',async()=>{
            try{
                const bicys = await Bicycle.allBicycle();
                expect(bicys.length).toBe(0)
                const a = Bicycle.createInstance(5,'Black','BMX',[6.765439, -75.888765]);
                const b = Bicycle.createInstance(1,'Black','Mountain',[6.765439, -75.888765]);
                await Bicycle.add(a);
                await Bicycle.add(b);

                const targetBicy = await Bicycle.findByCode(1);
                expect(targetBicy.code).toBe(b.code);
                expect(targetBicy.color).toBe(b.color);
                expect(targetBicy.model).toBe(b.model);
            }catch(err){
                console.log(err);
            }
            
        })
    })

    describe('Bicycle.removeByCode',()=>{
        it('must remove a bicycle',async()=>{
            try{
                let bicys = await Bicycle.allBicycle();
                expect(bicys.length).toBe(0);
                const a = Bicycle.createInstance(5,'Black','BMX',[6.765439, -75.888765]);
                const b = Bicycle.createInstance(1,'Black','Mountain',[6.765439, -75.888765]);
                await Bicycle.add(a);
                await Bicycle.add(b);
                await Bicycle.removeByCode(5);
                bicys = await Bicycle.allBicycle();
                expect(bicys.length).toBe(1);
                const targetBicy = await Bicycle.findByCode(5);
                expect(targetBicy).toBeNull();
            }catch(err){
                console.log(err);
            }
        })
    })
})










/*
beforeEach(()=> Bicycle.allBicycle = []);

describe('Bicycle.allBicycle',()=>{
    it('it´s void',()=>{
        expect(Bicycle.allBicycle.length).toBe(0)
    })
})

describe('Bicycle.add',()=>{
    it('add a Bicy',()=>{
        //Before
        expect(Bicycle.allBicycle.length).toBe(0);
        const bicy = new Bicycle(3,'Black','Mountain',[6.765439, -75.888765])
        Bicycle.add(bicy);
        //After
        expect(Bicycle.allBicycle.length).toBe(1)
        expect(Bicycle.allBicycle[0]).toBe(bicy)
    })
})

describe('Bicycle.findById',()=>{
    it('Must return a bicy with specific id',()=>{
        expect(Bicycle.allBicycle.length).toBe(0);
        const a = new Bicycle(3,'Black','Mountain',[6.765439, -75.888765])
        const b = new Bicycle(4,'Red','BMX',[5.767685, -65.871234])
        Bicycle.add(a);
        Bicycle.add(b);
        const bicy = Bicycle.findById(3);

        expect(bicy.id).toBe(a.id);
        expect(bicy.color).toBe(a.color);
        expect(bicy.model).toBe(a.model);
        
    })
})

describe('Bicycle.removeById',()=>{
    it('Must remove a bicy by ID',()=>{
        expect(Bicycle.allBicycle.length).toBe(0);
        const bicy = new Bicycle(1,"Black","Mountain",[5.767685, -65.871234]);
        Bicycle.add(bicy);
        expect(Bicycle.allBicycle.length).toBe(1);
        Bicycle.removeById(bicy.id);
        expect(Bicycle.allBicycle.length).toBe(0);
    })
})
*/
