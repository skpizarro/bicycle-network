const Bicycle = require('../../models/bicycle');
const request = require('request');
const mongoose = require('mongoose');
const server = require('../../bin/www');
const dbTest = require('../db_test');


var url_base = 'http://localhost:3000/api/bicycle';

describe('Bicycle API',()=>{

    beforeEach(async()=>{
        try{
            await dbTest.dbConnect();
        }catch(err){
            console.log(err);
        }
        
    })

    afterEach(async()=>{
        try{
            await Bicycle.deleteMany();
            console.log("Clear");
            await dbTest.dbDisconnect();
        }catch(err){
            console.log(err);
        }
        
    })

    describe('GET BICYCLE /',()=>{
        it('Status 200',(done)=>{
            request.get(url_base,(err,resp,body)=>{
                const result = JSON.parse(body);
                expect(resp.statusCode).toBe(200);
                expect(result.bicys.length).toBe(0);
                done();
            })
        })
    })
    
    describe('POST BICYCLE /create',()=>{
        it('STATUS 200',(done)=>{
            const headers = {'content-type':'application/json'}
            const aBicy = '{"code": 10,"color":"Gray","model":"BMX","lat":6.765439,"lng":-75.888765}'
            
            request.post({
                headers,
                url: `${url_base}/create`,
                body:aBicy
            },(err,resp,body)=>{
                const bicy = JSON.parse(body).bicy;
                expect(resp.statusCode).toBe(200);
                expect(bicy.color).toBe("Gray");
                expect(bicy.location[0]).toBe(6.765439);
                expect(bicy.location[1]).toBe(-75.888765);
                done();
            })
    
        })
    })

    describe('DELETE BICYCLE /delete',()=>{
        it('Status 200',async(done)=>{
            
            const aBicy = Bicycle.createInstance(5,'Black','Mountain',[6.765439, -75.888765]);
            const newBicy = await Bicycle.add(aBicy);
            //console.log(newBicy.id);
            expect(Bicycle.allBicycle.length).toBe(1);
            

            const headers ={'content-type':'application/json'}
            const url = `${url_base}/delete`
            const id = {"id":newBicy.id}
            //console.log(id);
            
            request.delete({
                headers,
                url,
                body: id,
                json:true
            },(err,resp,body)=>{
                if(err) console.log(err);
                expect(resp.statusCode).toBe(200);
                //expect(Bicycle.allBicycle.length).toBe(0);
                done();
            })
        })
    })

    describe('UPDATE BICYCLE /update',()=>{
        it('status 200',async (done)=>{
            //expect(Bicycle.allBicycle.length).toBe(0)
            const a = await Bicycle.createInstance(1,"Blue","BMX",[6.765439, -75.888765]);
            const b = await Bicycle.createInstance(2,"Gray","Urban",[5.765439, -55.888765]);
             
            await Bicycle.add(a);
            await Bicycle.add(b);
            
            const newBicy = {"id":b.id,"code":3,"color":"Yellow","model":"Mountain","lat":5.29543,"lng":-77.34987};
            const headers = {"content-type":"application/json"};
            const url = `${url_base}/update`;

            request.put({
                headers,
                url,
                body:newBicy,
                json:true
            },async(err,resp,body)=>{
                const bicy = await Bicycle.findById(b.id)
                expect(bicy.color).toBe("Yellow");
                expect(resp.statusCode).toBe(200);
                done();
            })

        })
    })
})
