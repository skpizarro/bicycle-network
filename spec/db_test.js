const mongoose = require('mongoose');

const dbConnect =async ()=>{
    await mongoose.connect('mongodb://localhost/testdb',{
        useNewUrlParser:true,
        useUnifiedTopology:true,
        useCreateIndex: true
        },
        (err,res)=>{
            if(err) console.log(err);
            console.log("We are connected from test database!");
            //done();
        })
}

const dbDisconnect = async()=>{
   await mongoose.connection.close();
    console.log("Disconnected from database");
}

module.exports = {dbConnect,dbDisconnect}
