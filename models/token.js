const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const tokenSchema = new Schema({
    _userId: {
        type: Schema.Types.ObjectId,
        required:true,
        ref:'User'},
    token:{
        type:String,
        required:true
    },
    createAt:{
        type:Date,
        required:true,
        default: Date.now,
        expires: 43200
    }
})

// tokenSchema.methods.confirmToken = async(token)=>{
//     try {
//         const Token = await tokenSchema.findOne({token})
//         console.log(Token);
//         if(!Token) return false
//         return {token:Token}
//     } catch (error) {
//         return console.log(err);
//     }
// }

module.exports = mongoose.model('Token',tokenSchema);