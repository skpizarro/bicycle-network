const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const bicycleSchema = new Schema({
    code: Number,
    color: String,
    model: String,
    location:{
        type: [Number],
        index:{type:'2disphere',sparse:true}
    }
})

bicycleSchema.statics.createInstance = function(code,color,model,location){
    return new this({
        code,
        color,
        model,
        location
    })
}

bicycleSchema.methods.toString = function(){
    return `code: ${this.code} | color: ${this.color}`
}

bicycleSchema.statics.allBicycle = function(cb){
    return this.find({},cb);
}

bicycleSchema.statics.add = function(aBicy,cb){
    return this.create(aBicy,cb);
}

bicycleSchema.statics.findByCode = function(aCode,cb){
    return this.findOne({code:aCode},cb);
}

bicycleSchema.statics.removeByCode = function(aCode,cb){
    return this.deleteOne({code:aCode},cb);
}

bicycleSchema.statics.removeById = function(id,cb){
    return this.deleteOne({_id:id},cb);
}

// bicycleSchema.statics.findById = function(id,cb){
//     return this.findOne({_id:id},cb);
// }



module.exports = mongoose.model('Bicycle',bicycleSchema);


// const Bicycle = function(id,color,model,location){
//     this.id=id;
//     this.color=color;
//     this.model=model;
//     this.location=location;
// }

// Bicycle.prototype.toString = function(){
//     return 'id: '+ this.id +'| color: '+ this.color;
// }

// Bicycle.allBicycle=[];
// Bicycle.add = function(aBicycle){
//     Bicycle.allBicycle.push(aBicycle);
// }

// Bicycle.findById = function(bicyId){
//     var aBicy = Bicycle.allBicycle.find(x => x.id == bicyId);
//     if(aBicy){
//         return aBicy
//     }else{
//         throw new Error (`The Bicycle with the ID ${bicyId} does not exist`)
//     }
// }

// Bicycle.removeById = (bicyId)=>{
//     const aBicy = Bicycle.findById(bicyId);
//     if(aBicy){
//         let newAllBicy = Bicycle.allBicycle.filter(x => x != aBicy);
//         Bicycle.allBicycle = newAllBicy;
//     }
// }


// var a = new Bicycle(1,'Blue','Urban',[6.313408, -75.563629]);
// var b = new Bicycle(2,'Red','BMX',[6.314501, -75.563211]);

// Bicycle.add(a)
// Bicycle.add(b)

//module.exports = Bicycle;
