const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
const User = require('./user');
const Bicycle = require('./bicycle');

const reservationSchema = new Schema({
    since:Date,
    until:Date,
    bicycle:{type: Schema.Types.ObjectId, ref:'Bicycle'},
    user:{type: Schema.Types.ObjectId, ref:'User'}
});

reservationSchema.methods.reservationDays = function(){
    return moment(this.until).diff(moment(this.since),'days') + 1;
}

module.exports = mongoose.model('Reservation',reservationSchema);