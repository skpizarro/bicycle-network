const mongoose = require('mongoose');
const Reservation =require('./reservation');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto')
const UniqueValidator = require('mongoose-unique-validator');
const {mailConfig} = require('../mailer/mailer');
const Token = require('../models/token');

const saltRounds = 10;

const ValidateEmail= (email)=>{
 const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email)
}


const userSchema = new Schema({
    name: {
        type:String,
        trim:true,
        required: [true,'The name is required']
    },
    email:{
        type:String,
        trim:true,
        required:[true,'The email is required'],
        lowercase:true,
        unique:true,
        validate: [ValidateEmail, 'please enter a valid value for the email'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password:{
        type:String,
        required: [true, 'The password is required']
    },
    passwordResetToken:String,
    passwordResetTokenExpires: Date,
    verified:{
        type:Boolean,
        default:false
    },
    googleId:String,
    facebookId: String
})

userSchema.plugin(UniqueValidator,{message:'The {PATH} must be unique'});

userSchema.pre('save',function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password,saltRounds)
    }
    next();
})

userSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password,this.password);
}

userSchema.methods.reserve = async function(bicyId,since,until){
    const reservation = new Reservation({user:this._id,bicycle:bicyId,since,until});
    await reservation.save();
    return true;
}

userSchema.methods.send_welcome_email = function(cb){
    console.log("methods, send welcome email",this.id);
    const token = new Token({_userId:this.id, token: crypto.randomBytes(16).toString('hex')})
    console.log('token',token);
    const email_destination = this.email;
    console.log("email destination ",email_destination);
    token.save(function(err){
        if(err){return console.log(err.message)}

        const mailOptions ={
            from: 'juanpablopizarro97@gmail.com',
            to:email_destination,
            subject: 'Account Verification',
            html:`<h1>Hello</h1>
                <br/>
                <br/>
                <p>Please, to verify your account click this link: </p>
                <br/>
                <a href="${process.env.HOST}/token/confirmation/${token.token}">Verify My Account.</a>`
        }
        if(mailConfig.type === 'local'){
            mailConfig.MAIL.sendMail(mailOptions,function(err){
                if(err){return console.log(err.message);}
                console.log(`A welcome email has been sent to: ${email_destination}.`);
            })
        }else{
            mailConfig.MAIL.send(mailOptions,function(err){
                if(err){return console.log(err.message);}
                console.log(`A welcome email has been sent to: ${email_destination}.`);
            })
        }
        
    })
}

userSchema.methods.resetPassword = async(user)=>{
    const email_destination = user.email;
    const token = await Token.findOne({_userId:user.id});
    console.log(token);
    if(!token) return false;
        
    const mailOptions ={
        from: 'juanpablopizarro97@gmail.com',
        to:email_destination,
        subject: 'Reset your password',
        html:`<h1>Hello</h1>
            <br/>
            <br/>
            <p>Please, to reset your passwprd click this link: </p>
            <br/>
            <a href="${process.env.HOST}/resetPassword/${token.token}">Reset My Password.</a>`
    }

    if(mailConfig.type === 'local'){
        mailConfig.MAIL.sendMail(mailOptions,function(err){
            if(err){return console.log(err.message);}
            console.log(`A reset password email has been sent to: ${email_destination}.`);
        })
    }else{
        mailConfig.MAIL.send(mailOptions,function(err){
            if(err){return console.log(err.message);}
            console.log(`A reset password email has been sent to: ${email_destination}.`);
        })
    }
    
    return true;
    
}

userSchema.methods.resetPasswordAPI = async(user)=>{
    const email_destination = user.email;
    const token = new Token({_userId:user.id, token: crypto.randomBytes(16).toString('hex')});
    const resp = await token.save();
    if(!resp) return false;
    const mailOptions ={
        from: 'juanpablopizarro97@gmail.com',
        to:email_destination,
        subject: 'Reset your password',
        html:`<h1>Hello</h1>
            <br/>
            <br/>
            <p>Please, to reset your passwprd click this link: </p>
            <br/>
            <a href="${process.env.HOST}/resetPassword/${token.token}">Reset My Password.</a>`
    }

    if(mailConfig.type === 'local'){
        mailConfig.MAIL.sendMail(mailOptions,function(err){
            if(err){return console.log(err.message);}
            console.log(`A reset password email has been sent to: ${email_destination}.`);
        })
    }else{
        mailConfig.MAIL.send(mailOptions,function(err){
            if(err){return console.log(err.message);}
            console.log(`A reset password email has been sent to: ${email_destination}.`);
        })
    }
    return true;
}

userSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(`ID:  ${condition.id}`);
    console.log(`EMAIL:  ${condition.emails[0].value}`);
    self.findOne({
        $or:[
            {'googleId':condition.id},{'email':condition.emails[0].value}
        ]
    }, (err,result)=>{
        if(result){
            callback(err,result)
        }else{
            console.log('------------CONDITION-------------');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.name = condition.displayName || 'WITH OUT NAME';
            values.verified = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('------------VALUES-------------');
            console.log(values);
            self.create(values,(err,result)=>{
                if(err) console.log(err);
                return callback(err,result)
            })
        }
    })
}

userSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(`ID:  ${condition.id}`);
    console.log(`EMAIL:  ${condition.emails[0].value}`);
    self.findOne({
        $or:[
            {'facebookId':condition.id},{'email':condition.emails[0].value}
        ]
    }, (err,result)=>{
        if(result){
            callback(err,result)
        }else{
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.name = condition.displayName || 'WITH OUT NAME';
            values.verified = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('------------VALUES-------------');
            console.log(values);
            self.create(values,(err,result)=>{
                if(err) console.log(err);
                return callback(err,result)
            })
        }
    })
}




module.exports = mongoose.model('User',userSchema);