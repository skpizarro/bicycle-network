var map = L.map('main_map').setView([6.313801,-75.564238], 16);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
}).addTo(map);

// L.marker([6.313801,-75.564238]).addTo(map);
// L.marker([6.313802,-75.564321]).addTo(map);
// L.marker([6.313804,-75.564876]).addTo(map);

$.ajax({
    dataType:"json",
    url:"/api/bicycle",
    success:(result)=>{
        console.log(result);
        result.bicycle.forEach( bicy => {
            L.marker(bicy.location,{title:bicy.id}).addTo(map);
            
        });
    }
})