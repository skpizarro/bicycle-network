const passport = require("passport");
const User = require('../models/user');
const Token = require('../models/token');

const login_get =(req,res)=>{
    res.render('session/login');
}

const login_post =(req,res,next)=>{
    passport.authenticate('local-signin',(err,user,info)=>{
        if(err) return next(err);
        console.log(info);
        if(!user) return res.render('session/login',{info});
        req.logIn(user,(err)=>{
            if(err) return next(err);
            return res.redirect('/')
        })
    })(req,res,next);
}

const logout =(req,res)=>{
    req.logout();
    res.redirect('/');
}

const forgotPass_get=(req,res)=>{
    res.render('session/forgotPassword');
}

const forgotPass_post=async(req,res)=>{
    try{
        const user = await User.findOne({email:req.body.email})
        console.log(user);
        if(!user){
            return res.render('session/forgotPassword',{err:"Don't exist an user with this email address"});
        }
        const resp = await user.resetPassword(user);
        if(!resp) return res.render('session/forgotPassword',{err:"Your token is not valid currently"});
        
        res.render('session/forgotPasswordMessage');
    }catch(err){
        return console.log(err);
    }
    
}


const resetPassword_get=async(req,res)=>{
    try {
        const token = await Token.findOne({token:req.params.token}).populate('_userId').exec();
        console.log(token._userId.name);
        if(token){
            res.render('session/resetPassword',{token:token.token,userName:token._userId.name})
        }
    } catch (error) {
        res.send('Error resetPassword')
    }
}

const resetPassword_post=async(req,res)=>{
    try {
        const token = req.params.token;
        const {newPassword,confirmNewPassword,name} = req.body;
        console.log(token);
        if(newPassword !== confirmNewPassword) return res.render(`session/resetPassword`,{token,userName:name,err:{message:"The new password and confirm password do not match"}})
        let resp = await Token.findOne({token}).populate('_userId').exec();
        const user = await User.findById(resp._userId);
        console.log("respuesta user",user);
        user.password = newPassword;
        await user.save();
        res.render('session/login');
    } catch (error) {
        res.send('Error resetPassword')
    }
}
module.exports ={login_get,login_post,logout,forgotPass_get,forgotPass_post,resetPassword_get,resetPassword_post}