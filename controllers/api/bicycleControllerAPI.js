const Bicycle = require('../../models/bicycle');

bicycle_list = async(req,res)=>{
    
    const bicys = await Bicycle.allBicycle()
    res.status(200).json({
        ok:true,
        bicys
    })
}


bicycle_create = async(req,res)=>{
   try{
        const {color,model,lat,lng} = req.body;
        const location = [lat,lng]
        const bicy = new Bicycle({color,model,location});

        await Bicycle.add(bicy);
        res.status(200).json({
            ok:true,
            bicy
        })
   }catch(err){
       console.log(err);
       res.status(400).json({
        ok:false,
        message:'ERROR'
    })
   }
}

bicycle_delete = async(req,res)=>{
    try{
        const id = req.body.id;
        console.log("controller",id);
        await Bicycle.removeById(id);
        res.status(200).json({
            ok:true,
            message: `Bycicle with ID ${id} was deleted`
        })
    }
    catch(err){
        res.status(400).json({
            ok:false,
            message:'ERROR'
        })
}
}

bicycle_update =async(req,res)=>{
    const {id,code,color,model,lat,lng} = req.body;
    const location = [lat,lng];
    const bicy = await Bicycle.findById(id);
    

    bicy.code = code;
    bicy.color = color;
    bicy.model = model;
    bicy.location =location;

    await bicy.save();
    
    res.status(200).json({
        ok:true,
        bicy
    })
}

module.exports ={bicycle_list,bicycle_create,bicycle_delete,bicycle_update} 