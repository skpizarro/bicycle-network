const User = require('../../models/user');

const users_list = async(req,res)=>{
    try{
        const users = await User.find();
        res.status(200).json({
            ok:true,
            users
        })
    }catch(err){
        res.status(400).json({
            ok:false,
            err
        })
    }
}

const user_create = async(req,res)=>{
    try {
        const {name,email,password} = req.body;
        const user = new User({name,email,password});
        const newUser = await user.save();
        res.status(200).json({
            ok:true,
            user:newUser
        })
    } catch (error) {
        res.status(400).json({
            ok:false,
            err:error,
            message: "User was not created"
        })
    }
}

const user_reservation =async(req,res)=>{
    try {
        const user = await User.findById(req.body.id);
        const reservation = await user.reserve(req.body.bicyId,req.body.since,req.body.until)
        console.log("reservation",reservation);
        if(reservation){
            console.log("Reservation complete!!");
            res.status(200).json({
                ok:true,
                message: "Reservation was Completed!!"
            })
        }
    } catch (error) {
        res.status(400).json({
            ok:false,
            message:"Reservation was not created"
        })
    }
}


module.exports ={users_list,user_create,user_reservation};