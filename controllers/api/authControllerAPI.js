const User = require('../../models/user');
const jwt = require('jsonwebtoken');



const authenticate =async(req,res,next)=>{
    try{
        const {email,password}=req.body;
        if(!email || !password) return res.status(401).json({
            ok:false,
            err:"Email and password are required"
        })
        const user = await User.findOne({email});
        if(!user) return res.status(401).json({
            ok:false,
            err:"Wrong Email or Password"
        })

        const validPass = await user.validPassword(password);
        console.log(validPass);
        if(!validPass){
            return res.status(401).json({
                ok:false,
                err:"Wrong Email or Password"
            })
        }
        const token = jwt.sign({user},process.env.SECRET_KEY,{expiresIn:'7d'});
        console.log(token);
        return res.status(200).json({
            ok:true,
            token
        })
    }catch(err){
        return res.status(400).json({
            ok:false,
            err
        })
    }
}

const forgotPassword =async(req,res,next)=>{
    try{
        const {email} = req.body;
        const user = await User.findOne({email});
        
        if(!user){
            return res.status(400).json({
                ok:false,
                err:`The email ${email} is not associated with any user`
            })
        }
        const resp = await user.resetPasswordAPI(user);
        console.log(resp);
        if(!resp) return res.status(400).json({
            ok:false,
            err:"Invalid Token to reset"
        })
        return res.status(200).json({
            ok:true,
            message:`A reset password email has been sent to: ${email}.`
        })
    }catch(err){
        console.log('ERRORRR');
        return res.status(400).json({
            ok:false,
            err
        })
    }
}


const authFacebookToken = (req,res,next)=>{
    const user = req.user;
    try{
        if(user){
            const token = jwt.sign({id:user.id},process.env.SECRET_KEY,{expiresIn:'7d'});
            res.status(200).json({
                data:{user,token},
                message:"User fond or created"
            })
        }else{
            res.status(401);
        }
    }catch(err){
        res.status(500).json({
            message:err.message
        })
    }
} 


module.exports ={authenticate,forgotPassword,authFacebookToken};