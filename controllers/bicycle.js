const Bicycle = require('../models/bicycle');

const bicycle_list = async(req,res)=>{
    try{
        const bicys = await Bicycle.allBicycle();
        res.render('./bicycle/index',{bicys})
    }catch(err){
        console.log(err);
    }
}


bicycle_create_get = function(req,res){
    res.render('./bicycle/create')
}

bicycle_create_post = function(req,res){
    const {id,color,model,lat,lng}= req.body
    const location =[lat,lng]
    const bicy = new Bicycle(id,color,model,location);
    Bicycle.add(bicy);

    res.redirect('/bicycle')
}


bicycle_update_get = function(req,res){
    const id = req.params.id;
    const bicy = Bicycle.findById(id)
    res.render('./bicycle/update',{bicy})
}

bicycle_update_post = function(req,res){
    const {id,color,model,lat,lng}= req.body
    const location =[lat,lng]
    const bicy = Bicycle.findById(id)
    bicy.id = id;
    bicy.color = color;
    bicy.model = model;
    bicy.location = location; 

    res.redirect('/bicycle')
}

bicycle_delete = (req,res)=>{
    const id = req.params.id;
    Bicycle.removeById(id);
    res.redirect('/bicycle');
}


module.exports={bicycle_list,bicycle_create_get,bicycle_create_post,bicycle_delete,bicycle_update_get,bicycle_update_post};
