const User = require('../models/user');

const user_list = async(req,res)=>{
    try{
        const users = await User.find();
        res.render("user/index",{users})
    }catch(err){
        console.log(err);
    }
}


const user_create_get = (req,res)=>{
    res.render('user/create',{errors:{},user:new User()})
}

const user_create = async(req,res)=>{
    const {name,email,password,confirm_password} = req.body

    console.log("body",name,email,password,confirm_password);
    try{
        if(password != confirm_password){
            console.log('No coincide el passs');
            return res.render('user/create',{errors:{confirm_password:{message:'The password does not match'}},user: new User({name,email})})
        }
        const newUser = await User.create({name,email,password});
        console.log("Usuario creado: ",newUser);
        await newUser.send_welcome_email();
        console.log("Email enviado");
        return res.redirect('/user');
    }catch(err){
        console.log("Error");
        return res.render('user/create',{errors: err.errors, user: new User({name,email})})
    }
   
}

const user_update_get = async(req,res)=>{
    try {
        const user = await User.findById(req.params.id)
        return res.render('user/update',{errors:{},user})
    } catch (error) {
        return console.log(error);
    }
}

const user_update = async(req,res)=>{
    const {name,email} = req.body;
    const id = req.params.id;
    try {
        const update_values = {name}
        const userUpdated = await User.findByIdAndUpdate(id,update_values);
        return res.redirect('/user')

    } catch (error) {
        console.log(error);
        res.render('user/update',{errors: err.errors, user: new User({name,email})})
    }
}

const user_delete = async(req,res,next)=>{
    try {
        await User.findByIdAndRemove(req.params.id);
        res.redirect('/user')
    } catch (error) {
        next(error);
    }

}


module.exports ={user_list,user_create_get,user_create,user_update_get,user_update,user_delete}