const Token = require('../models/token');
const User = require('../models/user');

const confirmationGet = async(req,res)=>{

    try{
        const token = await Token.findOne({token:req.params.token})
        if(!token)res.status(400).send({type:'not-verified',msg:'We cannot find a user with this token. Maybe it has expired and you should request a new token.'});
        let user = await User.findById(token._userId).exec();
        if(!user)res.status(400).send({type:'not-verified',msg:'We cannot find a user with this token.'});
        if(user.verified) return res.redirect('/user');
        user.verified = true;
        await user.save();
        return res.redirect('/')
    }catch(err){
        console.log(err);
        return res.status(500).send({msg:err.message})
    }
    
}

module.exports ={confirmationGet};