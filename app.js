//require('newrelic')
require('dotenv').config();
const {store} =require('./config/config');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('passport');
const session = require('express-session');

const {loggedIn} = require('./middlewares/login');
const {validateUser} = require('./middlewares/authAPI')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/user');
var bicycleRouter = require('./routes/bicycle')
var tokenRouter = require('./routes/token');
var loginRouter = require('./routes/login');
var bicycleAPIRouter = require('./routes/api/bicycle');
var userAPIRouter = require('./routes/api/user');
var authAPIRouter = require('./routes/api/auth');


var app = express();

require('./config/passport');

app.use(session({
  cookie: {maxAge: 240 * 60 * 60 * 1000},
  store,
  saveUninitialized:true,
  resave: true,
  secret:'bicycle_network++**!!judhys876474!|234??-_-'
}))


var mongoose = require('mongoose');
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, {useNewUrlParser:true,useUnifiedTopology: true, useCreateIndex:true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console,'MongoDB connection error: '));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

//ROUTES

app.use('/', indexRouter);
// Para efectos de prueba quité el middleware loggedIn de las rutas user, para que puedas crear un usuario tranquilamente
// Luego de crear el usuario, puedes comentar la linea 64 y descomentar la liena 63 para habilitar de nuevo el middleware
app.use('/user',loggedIn,usersRouter);
//app.use('/user',usersRouter);

app.use('/bicycle',loggedIn,bicycleRouter);
app.use('/token',tokenRouter);
app.use(loginRouter);

app.use('/api/bicycle',validateUser,bicycleAPIRouter);
app.use('/api/user',validateUser,userAPIRouter);
app.use('/api/auth',authAPIRouter);

app.use('/privacy_policy',(req,res)=>{
  res.sendFile('public/policy_privacy.html')
})

app.use('/googlea4864030b378f6c5',(req,res)=>{
  res.sendFile('public/googlea4864030b378f6c5.html')
})

app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email'
  ] 
}));

app.get('/auth/google/callback', 
  passport.authenticate('google',{ 
    successRedirect:'/',
    failureRedirect: '/error'
  })
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
