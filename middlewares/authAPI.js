const jwt  =require('jsonwebtoken');




const validateUser =(req,res,next)=>{
    jwt.verify(req.headers['x-access-token'],process.env.SECRET_KEY,(err,decoded)=>{
        if(err){
            return res.status(401).json({
                ok:false,
                err:{message:"Invalid token"}
            })
        }
        req.body.userId = decoded.id;
        next();
    })
}

module.exports = {validateUser}