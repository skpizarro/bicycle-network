const loggedIn =(req,res,next)=>{
    if(req.user) return next();
    console.log("User without login");
    res.render('session/login')
}
module.exports = {loggedIn};